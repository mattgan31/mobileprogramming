    console.log('*** C ***\n');

    console.log('### 1 ###');
    console.log('LOOPING PERTAMA');
    i = 1
    while (i <= 20) {
        if (i % 2 == 0) {
            console.log(i + ' - I love coding');
        }
        i++;
    }

    console.log('\nLOOPING KEDUA');
    while (i >= 1) {
        if (i % 2 == 0) {
            console.log(i + ' - I love coding');
        }
        i--;
    }

    console.log('\n');

    console.log('### 2 ###');


    for (i = 1; i <= 20; i++) {
        if (i % 3 == 0 && i % 2 == 1) {
            console.log(i + ' - I Love Coding');
        } else if (i % 2 == 1) {
            console.log(i + ' - Teknik');
        } else if (i % 2 == 0) {
            console.log(i + ' - Informatika');
        }
    }

    console.log('\n');

    console.log('### 3 ###\n');

    var persegi = '';
    for (i = 1; i <= 4; i++) {
        for (j = 1; j <= 8; j++) {
            persegi += '#';
        }
        persegi += '\n';
    }


    console.log(persegi);

    console.log('\n');

    console.log('### 4 ###\n');
    var tangga = '';
    for (i = 1; i <= 7; i++) {
        for (j = 1; j <= i; j++) {
            tangga += '#';
        }
        tangga += '\n';
    }
    console.log(tangga);

    console.log('\n');

    console.log('### 5 ###\n');
    var catur = '';
    for (i = 1; i <= 8; i++) {
        if (i % 2 == 0) {
            for (j = 1; j <= 8; j++) {
                if (j % 2 == 0) {
                    catur += '#';
                } else {
                    catur += ' ';
                }
            }
        } else {
            for (j = 1; j <= 8; j++) {
                if (j % 2 == 0) {
                    catur += ' ';
                } else {
                    catur += '#';
                }
            }
        }
        catur += '\n';
    }
    console.log(catur);