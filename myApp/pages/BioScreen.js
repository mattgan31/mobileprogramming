import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image } from 'react-native';

class LoginScreen extends React.Component  {
    render(){
      const { navigation } = this.props;
      return (
        <View style={styles.container}>
          <Image source={require('./../assets/images/profil.jpg')} style={styles.profil}/>
          <Text style={[styles.textPhoto,{
            top:355,
            fontSize:18,            
            left:35,
            color:"#fff",
            }]}>Rahmat Alamsyah</Text>
        <Text style={[styles.textPhoto,{
            top:380,
            fontSize:12,
            left:35,
            color:"#c0c0c0",
        }]}>+6281285028887</Text>
          <View
            style={[styles.content]}>
              <View style={{
                width:30,
                height:10,
                backgroundColor:"#ffffff33",                
                borderRadius:6,
                top: 15,
                alignSelf:'center'
              }}></View>
              <View style={{flexDirection:"row", top:35, left:35}}>
                <View style={[styles.blok, {backgroundColor:"#A9A9A9", width:130,}]}>
                  <Text style={{textAlign:'center', color:'black'}}>Teknik Informatika</Text>
                </View>
                <View style={[styles.blok, {backgroundColor:"#3A3A3A", width:60,}]}>
                  <Text style={{textAlign:'center', color:'white'}}>Pagi A</Text>
                </View>
                <View style={[styles.blok, {backgroundColor:"#0A80C8", width:68,}]}>
                  <Text style={{textAlign:'center', color:'white'}}>Laki-laki</Text>
                </View>
                <View style={[styles.blok, {backgroundColor:"#127D57", width:68}]}>
                  <Text style={{textAlign:'center', color:'white'}}>Islam</Text>
                </View>
              </View>
              <View style={{flexDirection:"row", top:35, marginTop:7, left:35}}>
                <View style={[styles.blok, {backgroundColor:"#241E1E", width:100,}]}>
                  <Text style={{textAlign:'center', color:'white'}}>Purwakarta</Text>
                </View>
                <View style={[styles.blok, {backgroundColor:"#043559", width:60,}]}>
                  <Text style={{textAlign:'center', color:'white'}}>Coding</Text>
                </View>
                <View style={[styles.blok, {backgroundColor:"#106103", width:98,}]}>
                  <Text style={{textAlign:'center', color:'white'}}>Programmer</Text>
                </View>
                <View style={[styles.blok, {backgroundColor:"#48a4b7", width:68}]}>
                  <Text style={{textAlign:'center', color:'white'}}>Pelajar</Text>
                </View>                
              </View>
              <View style={{flexDirection:"row", top:35, marginTop:7, left:35}}>
                <View style={[styles.blok, {backgroundColor:"#1DA1F2", width:120, flexDirection:'row', alignItems:'center'}]}>
                  <Image source={require('./../assets/images/tuiter.png')} style={{width:15, height:15, marginRight:5}}/>
                  <Text style={{textAlign:'center', color:'white'}}>alamsyahr00</Text>
                </View>
                <View style={[styles.blok, {backgroundColor:"#3b5998", width:110,flexDirection:'row'}]}>
                  <Image source={require('./../assets/images/fesbuk.png')} style={{width:15, height:15, marginRight:5}}/>
                  <Text style={{textAlign:'center', color:'white'}}>rahmataa1</Text>
                </View>                
              </View>
              <View style={{flexDirection:"row", top:35, marginTop:7, left:35}}>
                
                <View style={[styles.blok, {backgroundColor:"#bc2a8d", width:160,flexDirection:"row"}]}>
                  <Image source={require('./../assets/images/instagram.png')} style={{width:15, height:15, marginRight:5}}/>
                  <Text style={{textAlign:'center',color:'white'}}>rahmatalamsyah2</Text>
                </View>                
              </View>
              <Text style={[{
                color:'#fff',
                textAlign:'center',
                top:90,
                fontSize:12
              }]}>"Sukses bukanlah final, kegagalan tak terlalu fatal. {'\n'}Keberanian untuk melanjutkannya lah yang lebih penting" {'\n'}- Winston S. Churchill</Text>
          </View>
          <StatusBar style="light" hidden={false} translucent={true} />          
        </View>
      );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1d1f27',
    alignItems: 'center',
    justifyContent: 'center',
  },  
  teks:{
    color:"white",
  },
  profil:{
    zIndex:-1,
    alignItems:"center",
    width:411,
    height:636,
    top:-115,
    position:"absolute",
  },
  content:{
                backgroundColor:"#1d1f27",
                width:411,
                height:368,
                position:"absolute",
                borderTopLeftRadius:25,
                borderTopRightRadius:25,
                bottom:0,
    },
    textPhoto:{zIndex:2,
            position:"absolute",            
        },
    blok:{paddingLeft:5, 
        paddingRight:5,
        paddingTop:2, 
        paddingBottom:4, 
        borderRadius:3,
        marginRight:7,
        alignItems:'center',
        justifyContent:'center'
      }
});

export default LoginScreen;