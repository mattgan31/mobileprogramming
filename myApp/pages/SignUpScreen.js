import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image } from 'react-native';

class SignUpScreen extends React.Component {
  render (){
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <Image
        source={require('./../assets/images/logo-chitchat.png')}        
        style={styles.logo}
        />      
        <TextInput
        style={styles.input}
        placeholder = "Nama Pengguna"
        placeholderTextColor="#8b8d93"
        />
        <TextInput
        style={styles.input}
        placeholder = "Email"
        placeholderTextColor="#8b8d93"
        />
        <TextInput
        style={styles.input}
        secureTextEntry={true}
        placeholder = "Kata sandi"
        placeholderTextColor="#8b8d93"
        />
        <TextInput
        style={styles.input}
        secureTextEntry={true}
        placeholder = "Konfirmasi kata sandi"
        placeholderTextColor="#8b8d93"
        />      
        <TouchableOpacity style={styles.button}>
          <Text style={styles.teks}>Daftar</Text>
        </TouchableOpacity>
        <StatusBar style="light" hidden={false} translucent={true} />
        <View style={{flexDirection:"row", marginTop:5}}>
              <Text style={{color:"#fff", fontSize:12}}>Anda sudah bergabung ? </Text>
              <TouchableOpacity onPress={() => navigation.navigate('Login')}><Text style={{textDecorationLine:"underline", color:"#fff", fontSize:12}}>Masuk</Text></TouchableOpacity>
            </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1d1f27',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button:{
    alignItems:'center',
    justifyContent:'center',
    backgroundColor: '#1841f0',    
    paddingHorizontal : 15,
    paddingVertical: 5,
    marginTop:5,
    borderRadius:5,
    height:35,
    width:300,
  },
  input:{
        borderColor:"white",
        color:"white",
        borderWidth:1,
        borderRadius: 5,
        marginVertical:5,
        paddingLeft:5,
        paddingRight:20,
        width:300,
        height:35,
  },
  teks:{
    color:"white",
  },
  logo:{
    alignItems:"center",
    width:160,
    height:180,
    marginBottom:25
  }
});

export default SignUpScreen;