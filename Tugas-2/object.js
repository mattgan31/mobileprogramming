console.log('\n########## 1 ##########\n');
// No.1



var now = new Date()
var thisYear = now.getFullYear() // 2021 (tahun sekarang)

function arrayToObject(arr) {
    var no = 1;
    for (let i = 0; i < arr.length; i++) {
        var obj = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: thisYear - arr[i][3]
        }
        if (arr[i][3] > thisYear || arr[i][3] == null) {
            obj.age = "Invalid Birth Year";
        }
        console.log(no + '. ' + obj.firstName, obj.lastName + ' :', obj);
        var no = no + 1;
    }
}
// Driver Code
var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
]
arrayToObject(people)
/*
1. Bruce Banner: {
firstName: "Bruce",
lastName: "Banner",
gender: "male",
age: 45
}
2. Natasha Romanoff: {
firstName: "Natasha",
lastName: "Romanoff",
gender: "female".
age: "Invalid Birth Year"
}
*/

console.log('\n----------------------------\n')
var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]
arrayToObject(people2)
/*
1. Tony Stark: {
firstName: "Tony",
lastName: "Stark",
gender: "male",
age: 40
}
2. Pepper Pots: {
firstName: "Pepper",
lastName: "Pots",
gender: "female".
age: "Invalid Birth Year"
}
*/
// Error case
arrayToObject([]) // ""


console.log();

console.log('\n########## 2 ##########\n');
// No.2

function shoppingTime(memberId, money) {
    var belanja = {}
    var barang = [
        ['Sepatu Stacattu', 1500000],
        ['Baju Zoro', 500000],
        ['Baju H&N', 250000],
        ['Sweater Uniklooh', 175000],
        ['Casing Handphone', 50000]
    ]
    if (memberId == '' || memberId == null) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else {
        belanja.memberId = memberId;
    }

    if (money < 50000) {
        return "Mohon maaf, uang tidak cukup";
    } else {
        belanja.money = money;
    }
    var jumlahHargaBarang = 0;
    belanja.listPurchased = []
    for (let i = 0; i < barang.length; i++) {

        if (money > barang[i][1]) {
            belanja.listPurchased.push(barang[i][0])
            jumlahHargaBarang += barang[i][1]
        }
        belanja.changeMoney = money - jumlahHargaBarang
    }
    return belanja;
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
// [ 'Sepatu Stacattu',
// 'Baju Zoro',
// 'Baju H&N',
// 'Sweater Uniklooh',
// 'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
// [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log('\n########## 3 ##########\n');
// No.3

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here    
    var arrayHasil = [];
    for (let i = 0; i < arrPenumpang.length; i++) {
        if (arrPenumpang[i][1] == 'A') {
            var awal = 1;
        } else if (arrPenumpang[i][1] == 'B') {
            var awal = 2;
        } else if (arrPenumpang[i][1] == 'C') {
            var awal = 3;
        } else if (arrPenumpang[i][1] == 'D') {
            var awal = 4;
        } else if (arrPenumpang[i][1] == 'E') {
            var awal = 5;
        } else if (arrPenumpang[i][1] == 'F') {
            var awal = 6;
        }
        if (arrPenumpang[i][2] == 'A') {
            var akhir = 1;
        } else if (arrPenumpang[i][2] == 'B') {
            var akhir = 2;
        } else if (arrPenumpang[i][2] == 'C') {
            var akhir = 3;
        } else if (arrPenumpang[i][2] == 'D') {
            var akhir = 4;
        } else if (arrPenumpang[i][2] == 'E') {
            var akhir = 5;
        } else if (arrPenumpang[i][2] == 'F') {
            var akhir = 6;
        }

        var objAngkot = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: (akhir - awal) * 2000
        }

        arrayHasil.push(objAngkot);
        // console.log(objAngkot);        
    }
    return arrayHasil;
}
//TEST CASE
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
// { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
console.log(naikAngkot([])); //[]