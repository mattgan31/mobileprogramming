// Array
console.log('########## 1 ##########\n');
// No.1

function range(startNum, finishNum) {
    var a = [];

    if (startNum < finishNum) {
        for (let i = startNum; i <= finishNum; i++) {
            a.push(i);
        }
    } else if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i--) {
            a.push(i);
        }
    } else if (startNum == null || finishNum == null) {
        var a = -1;
    }
    return a;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

console.log('\n########## 2 ##########\n');
// No.2

function rangeWithStep(startNum, finishNum, step) {
    var a = [];

    if (startNum < finishNum) {
        for (let i = startNum; i <= finishNum; i += step) {
            a.push(i);
        }
    } else if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i -= step) {
            a.push(i);
        }
    } else if (startNum == null || finishNum == null) {
        a.push(-1);
    }
    return a;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

console.log('\n########## 3 ##########\n');
// No.3

// Code di sini
function sum(startNum, finishNum, step) {
    var a = [];
    if (startNum == null && finishNum == null && step == null) {
        a.push(0);
    } else if (startNum < finishNum) {
        if (step == null) {
            step = 1
        }
        for (let i = startNum; i <= finishNum; i += step) {
            a.push(i);
        }
    } else if (startNum > finishNum) {
        if (step == null) {
            step = 1
        }
        for (let i = startNum; i >= finishNum; i -= step) {
            a.push(i);
        }
    } else if (startNum == null || finishNum == null) {
        a.push(startNum);
    }

    let total = a.reduce((val, nilai) => {
        return val + nilai;
    }, 0)
    return total;
}
console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

console.log('\n########## 4 ##########\n');
// No.4

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989",
        "Membaca"
    ],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970",
        "Berkebun"
    ]
]

function dataHandling(arr) {
    for (let i = 0; i < arr.length; i++) {

        console.log('Nomor ID: ' + arr[i][0]);
        console.log('Nama Lengkap: ' + arr[i][1]);
        console.log('TTL: ' + arr[i][2], arr[i][3]); //Masih salah
        console.log('Hobi: ' + arr[i][4] + '\n');

    }
}

dataHandling(input);

console.log('\n########## 5 ##########\n');
// No.5

function balikKata(str) {
    var splitStr = str.split('');
    var reverseStr = splitStr.reverse();
    var joinStr = reverseStr.join('');

    return joinStr;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("Informatika")) // akitamrofnI
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Humanikers")) // srekinamuH ma I

console.log('\n########## 6 ##########\n');
// No.6

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989",
    "Membaca"
];

function dataHandling2(input) {
    input.splice(1, 1, "Roman Alamsyah Elsharawy");
    input.splice(2, 1, "Provinsi Bandar Lampung");
    input.splice(4, 1, "Pria");
    input.splice(5, 0, "SMA Internasional Metro");
    console.log(input);

    var bulan = input[3];


    bulan = bulan.split('/');
    bulan.shift();
    bulan.pop();
    bulan = bulan + '';
    switch (bulan) {
        case '01':

            console.log('Januari');
            break;
        case '02':

            console.log('Febuari');
            break;
        case '03':

            console.log('Maret');
            break;
        case '04':

            console.log('April');
            break;
        case '05':
            console.log('Mei');
            break;
        case '06':
            console.log('Juni');
            break;
        case '07':
            console.log('Juli');
            break;
        case '08':
            console.log('Agustus');
            break;
        case '09':
            console.log('September');
            break;
        case '10':
            console.log('Oktober');
            break;
        case '11':
            console.log('Desember');
            break;
        case '12':
            bulan = "Desember";
            break;
        default:
            console.log("Bulan Salah");
            break;
    }
    var tgl = input.splice(3, 1);
    tgl = tgl + '';
    tgl = tgl.split('/');
    dashTgl = tgl.join('-');
    tahun = tgl.splice(2);
    tahun = tahun.concat(tgl);
    console.log(tahun);


    console.log(dashTgl);
    input.splice(2);
    input = input.pop();
    input = input.split(' ');
    input.pop();
    console.log(input.join(' '))
}

dataHandling2(input);
/**
* keluaran yang diharapkan (pada console)
*
* ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung",
"21/05/1989", "Pria", "SMA Internasional Metro"]
* Mei
* ["1989", "21", "05"]
* 21-05-1989
* Roman Alamsyah
*/